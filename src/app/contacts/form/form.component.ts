import { Component, OnInit,Output,EventEmitter } from '@angular/core';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
	@Output() onSubmitted:EventEmitter<any> = new EventEmitter();
	@Output() onCancel:EventEmitter<any>=new EventEmitter();
 
  constructor() { }

  ngOnInit() {
  }
  onSubmit(newcontact){
	this.onSubmitted.emit(newcontact);
  }
	cancel(){
	this.onCancel.emit();
  }
}
