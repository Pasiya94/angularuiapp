import { Component, OnInit } from '@angular/core';
import 'rxjs/Rx';
@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.css']
})
export class ContactsComponent implements OnInit {
 isAddingNewContact:boolean=false;
contact=[];
  constructor() { }

  ngOnInit() {
    this.contact=[
      {
        name: 'Pasindu Purna',
        phoneNumber: '0710452620'
      }      
    ]
  }
onSubmit(newcontact){
  this.contact.push(newcontact);
  this.isAddingNewContact=false;
}
}
