import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule }   from '@angular/forms';

import { ContactsComponent } from './contacts.component';
import { FormComponent } from './form/form.component';
import { ProfilesComponent } from './profiles/profiles.component';


@NgModule({
  
  imports: [
    CommonModule,
    FormsModule,    
  ],
  exports: [
    ContactsComponent
  ],  
  declarations: [ContactsComponent,ProfilesComponent,FormComponent],  
 
})
export class ContactsModule { }
