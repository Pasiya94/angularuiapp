import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { ContactsComponent } from './contacts/contacts.component';
import { ContactsModule } from "./contacts/contacts.module";
import { AppService } from "./app.service";

@NgModule({
  declarations: [
    AppComponent   
  ],
  imports: [
    BrowserModule,
    ContactsModule,
	HttpClientModule,    
    HttpModule  
  ],
  providers: [AppService],
  bootstrap: [AppComponent]
})
export class AppModule { }
